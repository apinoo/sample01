#!/bin/bash

if [[ -z $DB_USER || -z $DB_PASS || -z $DB_NAME || -z $DB_HOST || -z $DB_PORT ]]; then
    echo "Error: Falta una o más variables de ambiente necesarias (DB_USER, DB_PASS, DB_NAME, DB_HOST, DB_PORT)"
    exit 1
fi

mysqldump --user="$DB_USER" --password="$DB_PASS" --host="$DB_HOST" --port="$DB_PORT" "$DB_NAME" | bzip2 > dump.bz2

if [ $? -eq 0 ]; then
    echo "Dump de base de datos completado exitosamente en el archivo dump.bz2"
else
    echo "Dump de la base de datos ha fallado"
    exit 1
fi

